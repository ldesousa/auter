Auter - automated E-R diagrams
==============================

This programs uses [SQLAlchemy](https://www.sqlalchemy.org/) to inspect a relational database and generate a
E-R diagram configuration for [Mermaid](https://mermaid-js.github.io/mermaid/).

Getting ready
-------------

Start by creating a virtual environment:

`python3 -m venv env`

`source env/bin/activate`

Install the necessary libraries:

`pip3 install -r requirements.txt`

At this stage you might need to install additional libraries specific to the
target DBMS. For instance, Postgres requires the `psycopg2` library:

`pip3 install psycopg2`

Create a new `config` file:

`cp config.dist config`

Open the `config` with your favourite file editor and modify the settings to
your needs. The connection string follows the [SQLAlchemy conventions](https://docs.sqlalchemy.org/en/14/core/engines.html).

`vim config`

Usage
-----

Pass a list of table names as arguments to `auter.py`. The programme creates
markdown for all of these and any other tables referring to them:

`python3 auter.py my_table my_other_table > out.mm`

Then use mermaid to create an image from the markdown file:

`mmdc -i out.mm -o out.png`

Finally display the image with a programme like `eog`:

`eog out.png`

Markdown embedding
------------------

By now, all the three major git-based software forges support mermaid diagrams embedded in Markdown
documents: [Gitea](https://github.com/go-gitea/gitea/issues/3340), [GitLab](https://about.gitlab.com/handbook/tools-and-tips/mermaid/) and [GitHub](https://github.blog/2022-02-14-include-diagrams-markdown-files-mermaid/). You can get the mermaid code directly in the command line with `auter`:

`python3 auter.py my_table my_other_table`

