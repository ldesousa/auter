import sys
import configparser
from sqlalchemy import create_engine, MetaData, Table

class ERDiagram:
    """
    Singleton class used to reverse engineer a database.
    """

    engine = None
    session = None
    metadata = None
    output = "erDiagram\n"
    tables = set()
    foreign_keys = []

    def __init__(self, uri_db, table_names=None, dbschema=None):
        """
        Parameters
        ----------
        uri_db : str
            Connection string to the database
        """

        engine = create_engine(uri_db,
            connect_args={'options': '-csearch_path={}'.format(dbschema)})

        self.metadata = MetaData()
        self.metadata.reflect(bind=engine)

        self.traverseTables(table_names)


    def traverseTables(self, table_names):

        for table_name in table_names:
            try:
                table = self.metadata.tables[table_name]
            except:
                print("Failed to retrieve meta-data for table {}. Is the name correct?".format(table_name))
                exit()
            self.tables.add(table)

            for fk in table.foreign_keys:
                self.foreign_keys.append(fk)
                self.tables.add(fk.constraint.referred_table)


    def drawTables(self):

        for table in self.tables:

            self.output = self.output + "  {} {{\n".format(table.name)

            for col_name in table.columns.keys():
                col = table.columns[col_name]
                # Mermaid dislikes schema names in types, and sometimes
                # SQLAlchemy goes berserk with column types.
                try:
                    col_type = str(col.type).split(".")
                    if (len(col_type) > 1):
                        col_type = col_type[1]
                    else:
                        col_type = col_type[0]
                except:
                    col_type = "unknown"
                # Mermaid can't deal with type lengths, e.g. VARCHAR(7)
                col_type = col_type.split('(')[0]
                self.output = self.output + "    {} {}\n".format(col_type, col_name)

            self.output = self.output + "  }\n"


    def drawRelations(self):
        """
        Mermaid relation notations
        |o	Zero or one
        ||	Exactly one
        }o	Zero or more (no upper limit)
        }|	One or more (no upper limit)
        """
        for fk in self.foreign_keys:

            if(fk.column.nullable):
                rel = "o"
            else:
                rel = "|"

            self.output = self.output + "  {} |{}--o{{ {} : {}\n".format(fk.constraint.table.name,
                       rel, fk.constraint.referred_table.name, fk.name)


    def print(self):

        self.drawTables()
        self.drawRelations()
        print(self.output)


if __name__ == "__main__":

    sys.argv.pop(0)
    if(len(sys.argv) < 1):
        print("Sorry, need at least a table name to generate an ER diagram")
        exit()

    config = configparser.ConfigParser()
    config.read('config')
    conn_string = config['Config']['Connection']
    schema_name = config['Config']['Schema']

    diagram = ERDiagram(conn_string, sys.argv, schema_name)
    diagram.print()
